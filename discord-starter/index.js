const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');

const commands = [{
    name: 'boot',
    description: 'Starts the game server!!'
}];

const rest = new REST({ version: '9' }).setToken('OTE3MTQwNDcxMTc0Nzk1Mzg0.Ya0Xnw.p_5A-mmnHx5-v-8rMzk_blKz2qw');

(async () => {
    try {
        console.log('Started refreshing application (/) commands.');

        await rest.put(
            Routes.applicationGuildCommands('917140471174795384', '697893391262023772'),
            { body: commands },
        );

        console.log('Successfully reloaded application (/) commands.');
    } catch (error) {
        console.error(error);
    }
})();

const { Client, Intents } = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
const axios = require('axios');

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('interactionCreate', async interaction => {
    if (!interaction.isCommand()) return;

    if (interaction.commandName === 'boot') {
        try {
            await axios.post('https://us-central1-orbital-ability-334113.cloudfunctions.net/minecraft-start');
            console.log('Successfully booted!');
            await interaction.reply('Booted!');
        } catch (error) {
            console.error(error);
            await interaction.reply('Boot stubbed his toe!');
        }
    }
});

client.login('OTE3MTQwNDcxMTc0Nzk1Mzg0.Ya0Xnw.p_5A-mmnHx5-v-8rMzk_blKz2qw');
