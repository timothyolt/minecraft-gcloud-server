/**
 * Stop the Minecraft server
 */
const http = require('http');
const Compute = require('@google-cloud/compute');
const compute = new Compute();
const zone = compute.zone('us-east1-b');
const vm = zone.vm('instance-1');

exports.stopInstance = async function stopInstance(req, res) {
    // Start the VM
    const zone = compute.zone('us-east1-b');
    const vm = zone.vm('instance-1');
    console.log('about to stop a VM');
    vm.stop(function(err, operation, apiResponse) {
        console.log('instance stop successfully');
    });
    res.status(200).send('Success stop instance');
};